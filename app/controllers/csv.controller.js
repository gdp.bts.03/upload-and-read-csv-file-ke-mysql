const db = require("../models");
const Post = db.posts;

const fs = require("fs");
const csv = require("fast-csv");

const CsvParser = require("json2csv").Parser;

const upload = async (req, res) => {
  try {
    if (req.file == undefined) {
      return res.status(400).send("Please upload a CSV file!");
    }

    let posts = [];
    let path = __basedir + "/casecsv/asset/uploads/" + req.file.filename;

    fs.createReadStream(path)
      .pipe(csv.parse({ headers: true }))
      .on("error", (error) => {
        throw error.message;
      })
      .on("data", (row) => {
        posts.push(row);
      })
      .on("end", () => {
        Post.bulkCreate(posts)
          .then(() => {
            res.status(200).send({
              message:
                "Uploaded the file successfully: " + req.file.originalname,
            });
          })
          .catch((error) => {
            res.status(500).send({
              message: "Fail to import data into database!",
              error: error.message,
            });
          });
      });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: "Could not upload the file: " + req.file.originalname,
    });
  }
};

const getPosts = (req, res) => {
  Post.findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials.",
      });
    });
};

const download = (req, res) =>{
  Post.findAll().then((objs) =>{
    let posts = [];

    objs.forEach((obj) =>{
      const{id, title, description, published} = obj;
      posts.push({id, title, description, published});
    });

    const csvFields = ["Id","Title","Description","Published"];
    const csvParser = new CsvParser({csvFields});
    const csvData = csvParser.parse(posts);

    res.setHeader("Content-Type","text/csv");
    res.setHeader("Content-Disposition","attachment; filename= contoh-download.csv");

    res.status(200).end(csvData);
  });
};

module.exports = {
  upload,
  getPosts,
  download
};

