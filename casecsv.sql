-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2021 at 09:57 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `casecsv`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `description`, `published`, `createdAt`, `updatedAt`) VALUES
(1, 'Node.js', 'ndieonfe', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23'),
(2, 'Express', 'reervre', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23'),
(3, 'CSV File', 'vrevee', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23'),
(4, 'MySQL', 'cevrevb', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23'),
(5, 'Rest API', 'cecwwee', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23'),
(6, 'fast-csv', 'werwefrg', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23'),
(7, 'File Upload', 'vvevbr', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23'),
(8, 'PostgreSQL', 'vrevevervr', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23'),
(9, 'Microservices', 'ververvrev', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23'),
(10, 'Import CSV', 'cfvgbtgbr', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23'),
(11, 'Upload CSV', 'btgbynuhmn', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23'),
(12, 'Download CSV', 'nhjmuhrtgt', 0, '2021-06-03 07:16:23', '2021-06-03 07:16:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
